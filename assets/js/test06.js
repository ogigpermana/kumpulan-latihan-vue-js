const PENGATURANWAKTU_STATES = {
    KERJA: 'kerja',
    ISTIRAHAT: 'istirahat'
};

const STATES = {
    MULAI: 'mulai',
    BERHENTI: 'berhenti',
    JEDA: 'jeda'
};

const DURASI_WAKTU_KERJA_DALAM_MENIT = 5;
const DURASI_WAKTU_ISTIRAHAT_DALAM_MENIT = 1;

new Vue({
    el: '#app',
    data: {
        state: STATES.MULAI,
        state: STATES.BERHENTI,
        state: STATES.JEDA,
        menit: DURASI_WAKTU_KERJA_DALAM_MENIT,
        detik: 0,
        pengaturanwaktuState: PENGATURANWAKTU_STATES.KERJA,
        timestamp: 0
    },
    computed: {
        // Penambahan notif kerja/istirahat saat pindah interval
        title: function () {
                return this.pengaturanwaktuState === PENGATURANWAKTU_STATES.KERJA ? 'Waktu Kerja!' :
                'Waktu Istirahat!'
            },
        // Penambahan left-pad untuk mempercantik tampilan menit
        men: function () {
            if (this.menit < 10) {
                return '0' + this.menit;
            }
            return this.menit;
        },
        // Penambahan left-pad untuk mempercantik tampilan detik
        det: function () {
                if (this.detik < 10) {
                return '0' + this.detik;
            }
            return this.detik;
        }
    },
    methods: {
        // Tombol start action
        start: function () {
            this.state = STATES.MULAI;
            this._tick();
            this.interval = setInterval(this._tick, 1000);
        },
        // Tombol jeda action
        pause: function () {
            this.state = STATES.JEDA;
            clearInterval(this.interval);
        },
        // Tombol berhenti action
        stop: function () {
            this.state = STATES.BERHENTI;
            clearInterval(this.interval);
            this.pengaturanwaktuState = PENGATURANWAKTU_STATES.KERJA;
            this.menit = DURASI_WAKTU_KERJA_DALAM_MENIT;
            this.detik = 0;
        },
        _tick: function () {
            //update timestamp yang digunakan pada gambar src
            if (this.detik % 10 === 0) {
                this.timestamp = new Date().getTime();
            }

            // Jika detik tidak sama dengan nol, maka kurangi waktu detik
            if (this.detik !== 0) {
                this.detik--;
                return;
            }
            //Jika detik sama dengan nol dan menit tidak sama dengan nol,
            //kurangi waktu menit dan atur detik ke waktu 59
            if (this.menit !== 0) {
                this.menit--;
                this.detik = 59;
                return;
            }
            //Jika detik sama dengan nol, dan menit sama dengan nol
            //Alihkan selang kerja/istirahat
            this.pengaturanwaktuState = this.pengaturanwaktuState ===
            PENGATURANWAKTU_STATES.KERJA ? PENGATURANWAKTU_STATES.ISTIRAHAT :
            PENGATURANWAKTU_STATES.KERJA;
            if (this.pengaturanwaktuState === PENGATURANWAKTU_STATES.KERJA) {
                this.menit = DURASI_WAKTU_KERJA_DALAM_MENIT;
            } else {
                this.menit = DURASI_WAKTU_ISTIRAHAT_DALAM_MENIT;
            }
        }
    }
});